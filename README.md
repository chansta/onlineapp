# Heroku Application 

This repository contains the projects I am hosting on [heroku](https://www.heroku.com). It includes the source file as well as additional file required by heroku. 

## RCorrelate

This is a [Rshiny](https://shiny.rstudio.com) application. Since heroku does not provide native support of [R](http://www.r-project.org), this is done via a third party buildpack. Details can be found within the directory. 

The main application is to demonstrate the concept of correlation. It simulates two random variables with user specified correlation. Users can change the means and variances of the two random variables in addition to the correlation.  

## correlateApp

This app is the [Bokeh](http://bokeh.pydata.org) version of RCorrelate. It also allows the two random variables to follow a bi-variate t-distribution in addition to a bi-variate normal distribution. 

## normalApp

This app demonstrates the normal and t-distirbutions. It allows users to change the means and varinaces of the two distributions. 
