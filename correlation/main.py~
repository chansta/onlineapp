##################################################################################################
"""
name:				main.py
author:				Felix Chan
email:				fmfchan@gmail.com
date created:			2020.03.09
description:			This is a small bokeh app to demonstrate the concept of correlation. 
"""
##################################################################################################
import numpy as np
import scipy.stats as sps
import bokeh.plotting as bkp
import bokeh.models as bkm
import bokeh.models.widgets as wig
import bokeh.layouts as bkl
import bokeh.io as bki


###########################################################################################################################################################################
"""
Need to add a new function to simulate both normal and student-t. We separate the simulation part from the update function. 
"""

def gen_random(dist='Normal'):
    m1,m2,s1,s2,cor = xm.value, ym.value, xs.value, ys.value, r.value
    m = np.array([m1,m2])
    cov = [[np.power(s1,2), cor*s1*s2],[cor*s1*s2, np.power(s2,2)]]
    s = sps.multivariate_normal.rvs(size=n, mean=m, cov=cov).transpose()
    if dist != 'Normal':
        denom = sps.chi2.rvs(size=n, df=degree.value)/degree.value
        s = s/np.power(denom, 0.5)
    return s


def update():
    s = gen_random(dist=dist_choice.value)
    est=np.corrcoef(s)[0][1]
    est_text.text = 'Sample correlation is {0:2f}'.format(est) 
    source.data=dict(x=s[0], y=s[1])

"""
Note that the globla variables have been shifted to the simulation function. 
"""
###########################################################################################################################################################################
"""
This section sets some of the parameters of the web application.  
    step: float in (0,1], general increment value. 
    max_mean: float. Maximum mean of the random variable allowed in the app. It is also used to define the plot range. 
    n: positive int. Number of observations to simulate. 
"""

step = 0.1
max_mean, max_s = 5,2
n = 5000
upper = max_mean+3*max_s
lower = -upper
###########################################################################################################################################################################
"""
This section defines the widgets used in the web app. There are total of five inputs. Slide bars for means and variances (two each) and a slide bar for correlation. 

In this case, two additional widgets have been added. One to choose between distributions and the other allows the input of additional parameters of the t-distribution. 
"""
xm = wig.Slider(title='Mean of x', start=-max_mean, end=max_mean, value=0, step=step)
ym = wig.Slider(title='Mean of y', start=-max_mean, end=max_mean, value=0, step=step)
xs = wig.Slider(title='Variance of x', start=0, end=max_s, value=1, step=step)
ys = wig.Slider(title='Variance of y', start=0, end=max_s, value=1, step=step)
r = wig.Slider(title='Correlation', start=-1, end=1, value=0, step=step/10)
dist_choice = wig.Select(title='Distribution', value='Normal', options=['Normal', 'Multivariate t'])
degree = wig.Slider(title='Degree of Freedom', start=2, end=20,  value=10, step=step)
controls = [xm,ym,xs,ys,r,dist_choice, degree]
###########################################################################################################################################################################
"""
This section initiates the plot with initial values from the weidget. It is essentially the same step as those in the update() function. 
"""

m1,m2,s1,s2,cor = xm.value, ym.value, xs.value, ys.value, r.value
m = np.array([m1,m2])
cov = np.diag([np.power(s1,2),np.power(s2,2)])
cov[0,1], cov[1,0] = r.value*s1*s2, r.value*s1*s2
s = sps.multivariate_normal.rvs(size=n, mean=m, cov=cov).transpose()
source = bkm.ColumnDataSource(data=dict(x=s[0],y=s[1]))
est = np.corrcoef(s)[0][1]

###########################################################################################################################################################################
# We may also include some explanation in the app. This is useful if text is required between widgets.  

control_text = bkm.Div(text='<h3>Controls</h3> <p>The following slidebars can be used to change the value of correlation and other features of the random variables. </p>', width=800, height=100)
t_text = bkm.Div(text='<h3>Additional Control</h3><p>This control is only relevant for the multivariate-t distribution</p>', width=800, height=100)
est_text = bkm.Paragraph(text='Sample correlation is {0:2f}'.format(est))

###########################################################################################################################################################################
"""
Initiating the original plot using the default values.
"""
p = bkp.figure(title='Correlation', plot_height=600, plot_width=800, x_range=(lower, upper), y_range=(lower,upper))
p.circle('x','y', source=source)
###########################################################################################################################################################################
"""
This section links the values of the widgets when they change to the update function. 
"""
for i,c in enumerate(controls):
    c.on_change('value', lambda attr, old, new:update())
###########################################################################################################################################################################
"""
This section creates the user-interface (UI) for the web application. 
"""
mean_control = bkl.row(xm,ym)
variance_control = bkl.row(xs,ys)
dist_control = bkl.row(dist_choice, r)
control_row = bkl.column(control_text, mean_control,variance_control, dist_control, est_text, t_text, degree) 
lay = bkl.row(p,control_row)
#lay = bkl.layout([[p], [control_text], [xm,ym], [xs,ys], [r]]) 

###########################################################################################################################################################################
update()
bki.curdoc().add_root(lay)




