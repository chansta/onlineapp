##################################################################################################
"""
name:				main.py
author:				Felix Chan
email:				fmfchan@gmail.com
date created:			2020.05.31
description:			Bokeh App to demonstrate the parameters in the normal distribution.
"""
##################################################################################################
import numpy as np
import scipy.stats as sps
import bokeh.plotting as bkp
import bokeh.models as bkm
import bokeh.models.widgets as wig
import bokeh.layouts as bkl
import bokeh.io as bki

def update():
    y = sps.norm.pdf(x, loc=m.value, scale=np.power(s.value, 0.5))
    z = sps.t.pdf(x, df=df.value, loc=mt.value) 
    info.text = '<h3>Summary</h3><p>The mean of the normal distribution is {0:1.3f} and the variance is {0:1.3f}.</p><p> The t-distribution has {2:1.3f} degree of freedom.</p>'.format(m.value, s.value, df.value)
    source.data=dict(x=x, y=y, z=z)

step = 0.1
max_mean, max_s = 5,2
n = 5000
upper = max_mean+3*max_s
lower = -upper

m = wig.Slider(title='Mean', start=-max_mean, end=max_mean, value=0, step=step)
s = wig.Slider(title='Variance', start=0, end=max_s, value=1, step=step)
mt = wig.Slider(title='Mean', start=-max_mean, end=max_mean, value=0, step=step)
df = wig.Slider(title='Degree of Freedom', start=1, end=30, value=5, step=step)
controls = [m,s, mt, df]

normal_control_text = bkm.Div(text='<h3>Normal Controls</h3> <p>The following slidebars can be used to change the values of mean and variance for the normal distribution. </p>', width=800, height=100)
t_control_text = bkm.Div(text='<h3>Student-t Controls</h3> <p>The following slidebars can be used to change the values of mean and the degree of freedom for the t-distribution. </p>', width=800, height=100)
info = bkm.Div(text='<h3>Summary</h3><p>The mean of the normal distribution is {0:1.3f} and the variance is {0:1.3f}.</p><p> The t-distribution has {2:1.3f} degree of freedom.</p>'.format(m.value, s.value, df.value))


#############################################################################3
#   Initial Plot
#############################################################################3
x = np.arange(lower, upper, step) 
y = sps.norm.pdf(x,loc=m.value, scale=np.power(s.value,0.5))
z = sps.t.pdf(x,loc=m.value, df=df.value)
source = bkm.ColumnDataSource(data=dict(x=x,y=y,z=z))
p = bkp.figure(title='Normal and Student-t Distributions', plot_height=600, plot_width=800, x_range=(lower, upper), y_range=(0,0.8))
p.line('x','y', source=source, legend_label='Normal')
p.line('x','z', source=source, color='red', legend_label='Student-t')
p.legend.location = 'top_right'
p.legend.click_policy='hide'

for i,c in enumerate(controls):
    c.on_change('value', lambda attr, old, new:update())

lay = bkl.row(p, bkl.column(normal_control_text, m,s, t_control_text, mt, df, info))
update()
bki.curdoc().add_root(lay)


